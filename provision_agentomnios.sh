#!/bin/bash

# Bootstrap a puppet agent node under OmniOS, for vagrant and otherwise.

PUPPET_BINDIR="/opt/local/bin"
PUPPET_PRIVBINDIR="/opt/local/bin"
PUPPET_CONFDIR="/etc/puppetlabs/puppet"
PUPPET_CODEDIR="/etc/puppetlabs/code"


if [[ $HOSTNAME =~ vagrant ]] ; then
    SCRIPT_PATH='/vagrant/provision'
else
    SCRIPT=$(readlink -f $0)
    SCRIPT_PATH=`dirname $SCRIPT`
fi
source ${SCRIPT_PATH}/params.sh || (echo "Can't find params.sh" && exit 1)
CWD=$(pwd)

# Add host entries for other vagrant boxes
if [[ $HOSTNAME =~ vagrant ]] ; then
    cat <<HOSTS >> /etc/hosts
192.168.137.10 xmaster.vagrant.vm xmaster puppet
192.168.137.12 xpuppetdb.vagrant.vm xpuppetdb puppetdb
192.168.137.13 xpuppetca.vagrant.vm xpuppetca puppetca
192.168.137.14 xagent.vagrant.vm xagent agentq
192.168.137.15 xagentomnios.vagrant.vm xagentomnios agentomnios
HOSTS
fi

# Update DNS config, if specified
if [ "${RESOLV_CONF}" ]; then
    echo "Updating resolv.conf..."
    echo "${RESOLV_CONF}" > /etc/resolv.conf
fi

# Create home dir
echo "Creating home directory zfs folder..."
zfs create -o mountpoint=/export rpool/export
zfs create rpool/export/home

# Remove upnp
sed -i '/upnp/d' /etc/passwd
sed -i '/upnp/d' /etc/shadow
sed -i '/upnp/d' /etc/group

# Set beadm auto naming

pkg set-property auto-be-name omniosce-r%r

##
# Install pkgsrc
##

mkdir -p /opt/pkgsrc
cd /opt/pkgsrc

echo "Downloading pkgsrc bootstrap..."

if [ ! -f ${BOOTSTRAP_TAR} ]; then
    # Download the bootstrap kit to the current directory.
    curl -O https://pkgsrc.joyent.com/packages/SmartOS/bootstrap/${BOOTSTRAP_TAR}
fi

# Verify the SHA1 checksum.
if [ "${BOOTSTRAP_SHA}" != "$(/bin/digest -a sha1 ${BOOTSTRAP_TAR})" ]; then
    echo "ERROR: pkgsrc checksum failure"
    exit 1
fi

echo "Installing pkgsrc bootstrap..."

# Install bootstrap kit to /opt/local
tar -zxpf ${BOOTSTRAP_TAR} -C /

# Add to PATH/MANPATH.
export PATH=/opt/local/sbin:/opt/local/bin:$PATH
export MANPATH=/opt/local/man:$MANPATH


echo "Installing package dependancies and puppet..."

# Install package dependencies
pkg install archiver/gnu-tar

#ln -s /opt/local/etc/puppetlabs /etc/puppetlabs

if [ -L /etc/puppetlabs ]; then
    /usr/gnu/bin/rm /etc/puppetlabs
fi

pkgin -y install ruby${RUBY_RELEASE}-gem_plugin

gem install puppet -v ${PUPPET_RELEASE_GEM}

echo "Configuring Puppet..."

mkdir -p ${PUPPET_CONFDIR}

# Point puppet agent to puppet master/CA
[ ! "${PUPPET_CA}" ] && PUPPET_CA=${PUPPET_MASTER}

cat <<PUPPET >> ${PUPPET_CONFDIR}/puppet.conf
[main]
    ca_server = ${PUPPET_CA}
[agent]
    server = ${PUPPET_MASTER}
    environment = ${PUPPET_ENVIRONMENT}
    preferred_serialization_format = pson
PUPPET


# This works around error on agent nodes when using non-production environment
# Ref: https://tickets.puppetlabs.com/browse/PUP-4954
mkdir -p ${PUPPET_ENVIRONMENTPATH}/${PUPPET_ENVIRONMENT}

# If present, copy in any custom RSA key for Vagrant root user.
mkdir -p ~/.ssh
chmod 700 ~/.ssh
if [[ $HOSTNAME =~ vagrant ]] && [ -f /vagrant/provision/keys/ssh/vagrant_root.rsa ]; then
    cp /vagrant/provision/keys/ssh/vagrant_root.rsa ~/.ssh/id_rsa
    chmod 0600 ~/.ssh/id_rsa
fi


echo "Installing control repo RSA key..."

# Install control repo RSA key
if [ "${PUPPET_CONTROLREPO_PRIVKEY}" ]; then
    echo "${PUPPET_CONTROLREPO_PRIVKEY}" > ~/.ssh/controlrepo_rsa
    chmod 0600 ~/.ssh/controlrepo_rsa
    cat <<SSHCONFIG >> ~/.ssh/config
Host ${PUPPET_CONTROLREPO_HOSTNAME}
        HostName ${PUPPET_CONTROLREPO_HOSTNAME}
        PreferredAuthentications publickey
        IdentityFile ~/.ssh/controlrepo_rsa
SSHCONFIG
fi

echo "Populatiing known_hosts file..."

# Populate known_hosts file
if [ "${PUPPET_CONTROLREPO_PUBKEY}" ]; then
    echo "${PUPPET_CONTROLREPO_PUBKEY}" >> ~/.ssh/known_hosts
else
    ssh-keyscan -t rsa ${PUPPET_CONTROLREPO_HOSTNAME} >> ~/.ssh/known_hosts 2>/dev/null
fi

# Install git and curl if not already present
#installpackage CSWgit
#installpackage CSWcurl

echo "Installing git..."

pkg install developer/versioning/git

echo "Syncing clock..."

ntpdate tick.wustl.edu

# Install a caching substitute for prtdiag

if [ ! -f /usr/sbin/prtdiag.real ]; then
    mv /usr/sbin/prtdiag /usr/sbin/prtdiag.real
    cp prtdiag.sh /usr/sbin/prtdiag.sh
    chmod +x /usr/sbin/prtdiag.sh
    ln -s /usr/sbin/prtdiag.sh /usr/sbin/prtdiag
    echo "Running prtdiag to fill cache"
    prtdiag 
    echo
fi

mkdir -p /var/NTP

# Finally, run puppet agent on myself

result=0
while [ $result -ne 2 ] ; do
    echo "Running puppet agent... Be sure to sign the key on ${PUPPET_CA}."
    echo "PATH=${PATH}:/usr/local/bin:${PUPPET_BINDIR} ${PUPPET_BINDIR}/puppet agent -t --verbose --waitforcert=120 --environment=${PUPPET_ENVIRONMENT}"
    echo
    ( PATH=${PATH}:/usr/local/bin:${PUPPET_BINDIR} ${PUPPET_BINDIR}/puppet agent -t --verbose --waitforcert=120 --environment=${PUPPET_ENVIRONMENT} 2>&1;      export result=$?;echo "Result code: $result" ) | tee -a /var/log/puppet_deploy.log
    echo
done

