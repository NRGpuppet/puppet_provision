#! /bin/bash

source params.sh

cd /opt/pkgsrc

UPGRADE_TAR="bootstrap-2020Q4-x86_64-upgrade.tar.gz"
UPGRADE_SHA="b73667b72e7fedac3c8ef5537451a9e52db28f4e"

die () {
    echo $1
    exit 1
}

# Download the upgrade kit to the current directory.
wget https://pkgsrc.joyent.com/packages/SmartOS/bootstrap-upgrade/${UPGRADE_TAR}
curl -O https://pkgsrc.joyent.com/packages/SmartOS/bootstrap-upgrade/${UPGRADE_TAR}
# Verify the SHA1 checksum.
[ "${UPGRADE_SHA}" == "$(/bin/digest -a sha1 ${UPGRADE_TAR})" ] || die "ERROR: checksum failure"

PKG_PATH=http://pkgsrc.joyent.com/packages/SmartOS/trunk/x86_64/All pkg_add -U pkg_install pkgin
tar -zxpf ${UPGRADE_TAR} -C / || die "Untar failed!"

pkgin -y remove ruby24-base

pkgin -y upgrade

# Fix mutt

pkgin -y remove dirmngr-1.1.1nb2 && pkgin -y install mutt

# Install Puppet
pkgin -y install ruby${RUBY_RELEASE}-gem_plugin
gem install puppet -v ${PUPPET_RELEASE_GEM}

