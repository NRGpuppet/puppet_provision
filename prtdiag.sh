#! /bin/bash
# prtdiag is very slow with lots of disks, breaking puppet
# This make a cache of the output the first time it runs and simply echos the cache later

cat /tmp/prtdiag.cache 2> /dev/null || prtdiag.real | tee /tmp/prtdiag.cache

