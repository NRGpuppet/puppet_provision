#!/bin/bash

# Bootstrap a puppet node under CentOS 6/7, for all platforms including vagrant.

HOSTNAME=`hostname`
if [[ $HOSTNAME =~ vagrant ]]; then
    SCRIPT_PATH='/vagrant/provision'
else
    SCRIPT=$(readlink -f $0)
    SCRIPT_PATH=`dirname $SCRIPT`
fi
source ${SCRIPT_PATH}/params.sh || (echo "Can't find params.sh" && exit 1)
OSRELEASE=$(rpm -qa \*-release | grep -Ei "redhat|centos" | cut -d"-" -f3)

case "$OSRELEASE" in
    '7' )
        PUPPET_RELEASE_RPM="${PUPPET_RELEASE_RPM_EL7}"
        ;;
    '6' )
        PUPPET_RELEASE_RPM="${PUPPET_RELEASE_RPM_EL6}"
        ;;
esac

# Download and install puppet-agent if not already present
if [ ! -d "${PUPPET_BINDIR}" ]; then
    rpm -Uvh "${PUPPET_RELEASE_RPM}"
    yum -y install puppet-agent
fi

# Store any role specified in a custom fact
if [ "$1" ]; then
    mkdir -p /etc/facter/facts.d/
    echo "role=${1}" > /etc/facter/facts.d/role.txt
fi

# Assuming puppetserver is up; run puppet agent on myself
echo "${HOSTNAME} is requesting a certificate from the puppetserver, please sign it within 120 secs."
( ${PUPPET_BINDIR}/puppet agent -t --verbose --waitforcert=120 --environment=${PUPPET_ENVIRONMENT} 2>&1 ; echo "Result code: $?" ) | tee -a /var/log/puppet_deploy.log
