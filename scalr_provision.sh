#!/bin/bash

# Puppet provisioning script for a CentOS 6/7 AWS instance under Scalr.
#
# This script assumes the machine's FQDN is set and already resolvable, whether a public
# name or an internal name like ip-XX-XX-XX-XX.ec2.internal.
#
# This script is intended to be triggered by a Scalr BeforeHostUp event on a newly launched
# machine, and with the following environment variables populated by Scalr.  Except where
# noted, all off these custom variables must be defined.
#
# PUPPET_MASTER = FQDN or IP address of puppet master
# PUPPET_CA = for now, same as PUPPET_MASTER
# PUPPET_CONTROLREPO_URL = "git@bitbucket.org/NRGpuppet/puppet_control_public"
# PUPPET_CONTROLREPO_HOSTNAME = "bitbucket.org"
# PUPPET_CONTROLREPO_PRIVKEY = private RSA key granting access to the control_repo
# PUPPET_CONTROLREPO_PUBKEY = control_repo public RSA key, for known_hosts file, optional
# PUPPET_PROJECT = a project tier for hiera, optional
# PUPPET_LOCATION = a location tier for hiera, optional
#
# These additional, optional environment variables can be used to pre-populate a keypair
# for hiera EYAML content.
#
# PUPPET_EYAML_PUBKEY = public PKCS7 EYAML key
# PUPPET_EYAML_PRIVKEY = private PKCS7 EYAML key
#
# Furthermore, environment variables can also override additional values, e.g.
# PUPPET_ENVIRONMENT, which are otherwise specifiedin the params.sh this script includes.
#
# For reference, here are the environment vars already provided by Scalr:
# https://scalr-wiki.atlassian.net/wiki/display/docs/Server-Scope+Global+Variables

# Script parameters:
#
# %puppet_role% = role class this machine will have, required.  For example
#   role::puppet::postgres_server.  It is prefered to only point this param to a
#   class in the Role module in this control_repo.  If %puppet_role% = no_role,
#   this script will update puppet.conf to point to PUPPET_MASTER and PUPPET_CA,
#   enable the puppet agent (i.e. for periodic cron run), but NOT launch any
#   a bootstrap script to actually run the puppet agent.
#
# This script will attempt to pull down the puppet_control repo specified, and then
# launch bootstrap scripts retrieved from that repo, per the %puppet_role% script
# parameter.
#
# Further Reference:
# https://scalr-wiki.atlassian.net/wiki/display/docs/Using+Puppet+with+Scalr

# Install control repo RSA key
mkdir -p ~/.ssh
chmod 0700 ~/.ssh
if [ "${PUPPET_CONTROLREPO_PRIVKEY}" ]; then
    echo "${PUPPET_CONTROLREPO_PRIVKEY}" > ~/.ssh/controlrepo_rsa
    chmod 0600 ~/.ssh/controlrepo_rsa
    cat <<SSHCONFIG >> ~/.ssh/config
Host ${PUPPET_CONTROLREPO_HOSTNAME}
	HostName ${PUPPET_CONTROLREPO_HOSTNAME}
	PreferredAuthentications publickey
	IdentityFile ~/.ssh/controlrepo_rsa
SSHCONFIG
fi

# Populate known_hosts file
if [ "${PUPPET_CONTROLREPO_PUBKEY}" ]; then
    echo "${PUPPET_CONTROLREPO_PUBKEY}" >> ~/.ssh/known_hosts
else
    ssh-keyscan ${PUPPET_CONTROLREPO_HOSTNAME} >> ~/.ssh/known_hosts 2>/dev/null
fi

# Yum update, to appease yum_update repo
yum clean all
# don't do yum update, can freeze and block machine launch
#yum -y update | tee -a /var/log/puppet_deploy.log
mkdir -p /var/spool/check_yum_update
echo "$(( $(/bin/date +%s) / 86400 ))" > /var/spool/check_yum_update/last_update

# Update Puppet GPG Key
curl --remote-name --location https://yum.puppetlabs.com/RPM-GPG-KEY-puppet
rpm --import RPM-GPG-KEY-puppet

# Install git and curl if not already present
yum -y install git curl

# Retrieve updated control_repo
PUPPET_CONTROLREPO_URL_TRIMMED=$(echo ${PUPPET_CONTROLREPO_URL} | sed s/'ssh:\/\/'/''/g)
rm -rf ~/puppet_control
git clone ${PUPPET_CONTROLREPO_URL_TRIMMED} ~/puppet_control

# Pull in default provision params
SCRIPT_PATH="${HOME}/puppet_control/provision" 
source ${SCRIPT_PATH}/params.sh || (echo "Can't find params.sh" && exit 1)
OSRELEASE=$(rpm -qa \*-release | grep -Ei "redhat|centos" | cut -d"-" -f3)
CWD=$(pwd)

case "$OSRELEASE" in
    '7' )
        PUPPET_RELEASE_RPM="${PUPPET_RELEASE_RPM_EL7}"
        ;;
    '6' )
        PUPPET_RELEASE_RPM="${PUPPET_RELEASE_RPM_EL6}"
        ;;
esac

# A reasonable PATH
echo "export PATH=${PATH}:/usr/local/bin:${PUPPET_BINDIR}" >> /etc/bashrc

# Install Chrony to force time sync
# Note these commands are CentOS 7 specific
yum install -y chrony
systemctl enable chronyd
systemctl start chronyd

# Download and install puppet-agent if not already present
if [ ! -d "${PUPPET_BINDIR}" ]; then
    rpm -Uvh "${PUPPET_RELEASE_RPM}"
    yum -y install puppet-agent
fi

# Point puppet agent to puppet master/CA
[ ! "${PUPPET_CA}" ] && PUPPET_CA=${PUPPET_MASTER}
cat <<PUPPET >> ${PUPPET_CONFDIR}/puppet.conf
[main]
    ca_server = ${PUPPET_CA}
[agent]
    server = ${PUPPET_MASTER}
PUPPET

# This works around error on agent nodes when using non-production environment
# Ref: https://tickets.puppetlabs.com/browse/PUP-4954
mkdir -p ${PUPPET_ENVIRONMENTPATH}/${PUPPET_ENVIRONMENT}

# Update EYAML keys if I am puppetserver or puppet CA
if [ "%puppet_role%" == 'role::puppet::puppetca' ] || [[ "%puppet_role%" =~ 'role::puppet::puppetserver' ]]; then

    mkdir -p ${PUPPET_KEYSDIR}
    # If defined, copy in EYAML keys from environment vars
    if [ "${PUPPET_EYAML_PUBKEY}" ] && [ "${PUPPET_EYAML_PUBKEY}" ]; then
	echo "${PUPPET_EYAML_PUBKEY}" > ${PUPPET_KEYSDIR}/public_key.pkcs7.pem
	echo "${PUPPET_EYAML_PRIVKEY}" > ${PUPPET_KEYSDIR}/private_key.pkcs7.pem
    fi
    chmod -R 0400 ${PUPPET_KEYSDIR}/*.pem
    chmod 0500 ${PUPPET_KEYSDIR}

fi

# Create custom facts from params & variables supplied by Scalr, so they're available to hiera
mkdir -p /etc/facter/facts.d/
[ "%puppet_role%" ] && [ "%puppet_role%" != 'no_role' ] && echo "role=%puppet_role%" >> /etc/facter/facts.d/role.txt
echo "puppet_server=${PUPPET_MASTER}" >> /etc/facter/facts.d/puppet_server.txt
echo "puppet_ca_server=${PUPPET_CA}" >> /etc/facter/facts.d/puppet_ca_server.txt
[ "${PUPPET_PROJECT}" ] && echo "project=${PUPPET_PROJECT}" >> /etc/facter/facts.d/project.txt
[ "${PUPPET_LOCATION}" ] && echo "location=${PUPPET_LOCATION}" >> /etc/facter/facts.d/location.txt

case "%puppet_role%" in
    role::puppet::puppetserver* | role::puppet::puppetca )
	## Bootstrap the puppetserver or CA
	${SCRIPT_PATH}/bootstrap_puppetserver.sh "%puppet_role%"
	;;
    no_role )
        ## Enable the puppet service but don't run bootstrap script
        ${PUPPET_BINDIR}/puppet resource service puppet ensure=running enable=true
	;;
    * )
	## Bootstrap a puppet node (including puppetdb)
	${SCRIPT_PATH}/bootstrap_agent.sh "%puppet_role%"
	;;
esac
