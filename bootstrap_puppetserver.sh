#!/bin/bash

# Bootstrap puppetserver or puppetserver CA 2.x (aka puppet v4) under CentOS, for all platforms including vagrant.

HOSTNAME=`hostname`
if [[ $HOSTNAME =~ vagrant ]]; then
    SCRIPT_PATH='/vagrant/provision'
else
    SCRIPT=$(readlink -f $0)
    SCRIPT_PATH=`dirname $SCRIPT`
fi
source ${SCRIPT_PATH}/params.sh || (echo "Can't find params.sh" && exit 1)
OSRELEASE=$(rpm -qa \*-release | grep -Ei "redhat|centos" | cut -d"-" -f3)

case "$OSRELEASE" in
    '7' )
        PUPPET_RELEASE_RPM="${PUPPET_RELEASE_RPM_EL7}"
        ;;
    '6' )
        PUPPET_RELEASE_RPM="${PUPPET_RELEASE_RPM_EL6}"
        ;;
esac

# Capture the exact puppetserver role class, if specified, to override default
ROLE="role::puppet::puppetserver_int_db"
[ "$1" ] && ROLE="$1"

# Download and install puppet-agent if not already present
if [ ! -d "${PUPPET_BINDIR}" ]; then
    rpm -Uvh "${PUPPET_RELEASE_RPM}"
    yum -y install puppet-agent
fi
# Install git and curl if not already present
yum -y install git curl

# Install r10k gem and deploy control repo for this environment
${PUPPET_PRIVBINDIR}/gem install r10k --no-ri --no-rdoc
mkdir -p ${PUPPET_R10KDIR}
cat > ${PUPPET_R10KDIR}/r10k.yaml <<R10K
:cachedir: '${PUPPET_VARDIR}/r10k'
:sources:
  :controlrepo:
    remote: '${PUPPET_CONTROLREPO_URL}'
    basedir: '${PUPPET_ENVIRONMENTPATH}'
R10K
rm -rf ${PUPPET_ENVIRONMENTPATH}/*
${PUPPET_PRIVBINDIR}/r10k deploy environment -pv info ${PUPPET_ENVIRONMENT}

# If one isn't already there, temporarily store role in a custom fact
mkdir -p /etc/facter/facts.d/
[ -f /etc/facter/facts.d/role.txt ] && ROLE_FACT_EXISTS="yes"
echo "role=${ROLE}" > /etc/facter/facts.d/role.txt

# Bootstrap hiera config before puppetserver
( ${PUPPET_BINDIR}/puppet apply -e "include profile::puppet::hiera" --verbose --environment=${PUPPET_ENVIRONMENT} 2>&1 ; echo "Result code: $?" ) | tee -a /var/log/puppet_deploy.log

# Now that puppet modules and hiera data/keys are in place, bootstrap puppetserver using role specified
( ${PUPPET_BINDIR}/puppet apply -e "include ${ROLE}" --verbose --environment=${PUPPET_ENVIRONMENT} 2>&1 ; echo "Result code: $?" ) | tee -a /var/log/puppet_deploy.log

if [ "${ROLE}" == 'role::puppet::puppetca' ]; then
    # Remaining steps for bootstrapping puppetserver CA

    # This seems to be necessary to ensure puppet SSL keys are in place
    ${PUPPET_BINDIR}/puppet agent -t --verbose --environment=${PUPPET_ENVIRONMENT}

    # Run manifest again to complete bootstrap
    ( ${PUPPET_BINDIR}/puppet apply -e "include ${ROLE}" --verbose --environment=${PUPPET_ENVIRONMENT} 2>&1 ; echo "Result code: $?" ) | tee -a /var/log/puppet_deploy.log
else
    # Remaining steps for bootstrapping puppetserver

    # Need to restart puppetserver for hiera update to take effect
    service puppetserver restart

    # Now do a full puppet agent run on myself
    ( ${PUPPET_BINDIR}/puppet agent -t --verbose --environment=${PUPPET_ENVIRONMENT} --waitforcert=120 2>&1 ; echo "Result code: $?" ) | tee -a /var/log/puppet_deploy.log
fi

# Remove the role fact, if was temporary
[ ! "${ROLE_FACT_EXISTS}" ] && rm -rf /etc/facter/facts.d/role.txt
