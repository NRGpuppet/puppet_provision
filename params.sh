#!/bin/bash

# Shared params used by provisioning scripts in bootstrapping puppetserver v2.x and puppet
# nodes, generally under CentOS, but not exclusively.  The params defined below are for
# use under Vagrant, and they can be overridden by global environment variables.  Likewise,
# other platforms (AWS, vSphere, etc) using the provisioning scripts can define these
# params as global environment vars as needed.
#
# Note the params in this first group are required.

[ ! "${PUPPET_RELEASE_RPM_EL7}" ] && PUPPET_RELEASE_RPM_EL7="https://yum.puppetlabs.com/puppetlabs-release-pc1-el-7.noarch.rpm"
[ ! "${PUPPET_RELEASE_RPM_EL6}" ] && PUPPET_RELEASE_RPM_EL6="https://yum.puppetlabs.com/puppetlabs-release-pc1-el-6.noarch.rpm"

# Puppet gem releases at: https://rubygems.org/gems/puppet/versions
[ ! "${PUPPET_RELEASE_GEM}" ] && PUPPET_RELEASE_GEM="5.5.22"
[ ! "${RUBY_RELEASE}" ] && RUBY_RELEASE="26"

[ ! "${PUPPET_BINDIR}" ] && PUPPET_BINDIR="/opt/puppetlabs/bin"
[ ! "${PUPPET_PRIVBINDIR}" ] && PUPPET_PRIVBINDIR="/opt/puppetlabs/puppet/bin"
[ ! "${PUPPET_CONFDIR}" ] && PUPPET_CONFDIR="/etc/puppetlabs/puppet"
[ ! "${PUPPET_CODEDIR}" ] && PUPPET_CODEDIR="/etc/puppetlabs/code"
[ ! "${PUPPET_VARDIR}" ] && PUPPET_VARDIR="/opt/puppetlabs/puppet/cache"
[ ! "${PUPPET_R10KDIR}" ] && PUPPET_R10KDIR="/etc/puppetlabs/r10k"
[ ! "${PUPPET_KEYSDIR}" ] && PUPPET_KEYSDIR="${PUPPET_CODEDIR}/keys"
[ ! "${PUPPET_ENVIRONMENT}" ] && PUPPET_ENVIRONMENT="production"
[ ! "${PUPPET_ENVIRONMENTPATH}" ] && PUPPET_ENVIRONMENTPATH="${PUPPET_CODEDIR}/environments"
[ ! "${PUPPET_CONTROLREPO_URL}" ] && PUPPET_CONTROLREPO_URL="ssh://hg@bitbucket.org/NRGpuppet/puppet_control"
[ ! "${PUPPET_CONTROLREPO_HOSTNAME}" ] && PUPPET_CONTROLREPO_HOSTNAME="bitbucket.org"
[ ! "${HOSTNAME}" ] && HOSTNAME=`hostname`

[ ! "${BOOTSTRAP_TAR}" ] && BOOTSTRAP_TAR="bootstrap-2020Q4-x86_64.tar.gz"
[ ! "${BOOTSTRAP_SHA}" ] && BOOTSTRAP_SHA="a36add0883d1b52be0190ff43361fd6abf22b144"


# Next these are optional pointers to the puppet master and CA, which will be seeded into
# puppet.conf. If the puppet master and CA are the same machine,
# comment out PUPPET_CA to leave it undefined.
[ ! "${PUPPET_MASTER}" ] && PUPPET_MASTER="puppet4.nrg.mir"
[ ! "${PUPPET_CA}" ] && PUPPET_CA="puppet4-ca.nrg.mir"

# Optional OpenCSW mirror for OmniOS
[ ! "${OPENCSW_MIRROR}" ] && OPENCSW_MIRROR="ftp://nrg-repo.nrg.mir/opencsw-full/testing/"

# Optional override for /etc/resolv.conf
#[ ! "${RESOLV_CONF}" ] && RESOLV_CONF="nameserver 10.28.16.20
#nameserver 10.28.16.21
#search nrg.mir"

# Finally, these optional values can populate root RSA keys for control-repo access, along
# with hiera-eyaml keys, on the puppet master and puppet CA machines.  This values can be
# specified here, or as global environment variables.
# PUPPET_CONTROLREPO_PRIVKEY=""
PUPPET_CONTROLREPO_PUBKEY="bitbucket.org ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAubiN81eDcafrgMeLzaFPsw2kNvEcqTKl/VqLat/MaB33pZy0y3rJZtnqwR2qOOvbwKZYKiEO1O6VqNEBxKvJJelCq0dTXWT5pbO2gDXC6h6QDXCaHo6pOHGPUy+YBaGQRGuSusMEASYiWunYN0vCAI8QaXnWMXNMdFP3jHAJH0eDsoiGnLPBlBp4TNm6rYI74nMzgz3B9IikW4WVK+dc8KZJZWYjAuORU3jc1c/NPskD2ASinf8v3xnfXeukU0sJ5N6m5E8VLjObPEO+mN2t/FZTMZLiFqPWc/ALSqnMnnhwrNi2rbfg/rd/IpL8Le3pSBne8+seeFVBoGqzHM9yXw=="
# PUPPET_EYAML_PUBKEY=""
# PUPPET_EYAML_PRIVKEY=""
#
# Note PUPPET_CONTROLREPO_PRIVKEY requires the param PUPPET_CONTROLREPO_HOSTNAME be
# defined, i.e. "bitbucket.org" or "github.com".
#
# Also, on Vagrant machines, any values specified for PUPPET_EYAML_PUBKEY and
# PUPPET_EYAML_PRIVKEY take precedence over keys placed in provision/keys/eyaml locally.
