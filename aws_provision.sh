#!/bin/bash

# Provisioning script for CentOS 7 Puppet VM(s) under AWS.

SCRIPT=$(readlink -f $0)
SCRIPT_PATH=`dirname $SCRIPT`
source ${SCRIPT_PATH}/params.sh || (echo "Can't find params.sh" && exit 1)
CWD=$(pwd)

# Restart syslog to reflect new hostname
systemctl restart rsyslog

# A reasonable PATH
echo "export PATH=$PATH:/usr/local/bin:/opt/puppetlabs/bin" >> /etc/bashrc

# # Add host entries for each system
# cat > /etc/hosts <<EOH
# 127.0.0.1 localhost localhost.localdomain localhost4 localhost4.localdomain
# ::1 localhost localhost.localdomain localhost6 localhost6.localdomain
# 192.168.137.10 xmaster.vagrant.vm xmaster puppet
# 192.168.137.12 xpuppetdb.vagrant.vm xpuppetdb puppetdb
# 192.168.137.13 xpuppetca.vagrant.vm xpuppetca puppetca
# 192.168.137.14 xagent.vagrant.vm xagent agent
# EOH

# Install Chrony to force time sync
# Note these commands are CentOS 7 specific
yum install -y chrony
systemctl enable chronyd
systemctl start chronyd

# Yum update, to appease yum_update repo
yum clean all
# don't do yum update, can freeze and block machine launch
#yum -y update | tee -a /var/log/puppet_deploy.log
mkdir -p /var/spool/check_yum_update
echo "$(( $(/bin/date +%s) / 86400 ))" > /var/spool/check_yum_update/last_update

# Update Puppet GPG Key
curl --remote-name --location https://yum.puppetlabs.com/RPM-GPG-KEY-puppet
rpm --import RPM-GPG-KEY-puppet

# Download and install puppet-agent if not already present
if [ ! -d "${PUPPET_BINDIR}" ]; then
    rpm -Uvh "${PUPPET_RELEASE_RPM}"
    yum -y install puppet-agent
fi

# Point puppet agent to puppet master/CA
[ ! "${PUPPET_CA}" ] && PUPPET_CA=${PUPPET_MASTER}
cat <<PUPPET >> ${PUPPET_CONFDIR}/puppet.conf
[main]
    ca_server = ${PUPPET_CA}
[agent]
    server = ${PUPPET_MASTER}
PUPPET

# # Open the firwall on inter-VM network traffic (assumed interface name enp0s8)
# # Note these commands are CentOS 7 specific
# systemctl enable firewalld
# service firewalld start
# firewall-cmd --zone=trusted --change-interface=enp0s8
# firewall-cmd --runtime-to-permanent

# This works around error on agent nodes when using non-production environment
# Ref: https://tickets.puppetlabs.com/browse/PUP-4954
mkdir -p ${PUPPET_ENVIRONMENTPATH}/${PUPPET_ENVIRONMENT}

mkdir -p /root/.ssh
chmod 0700 /root/.ssh

# Update keys and refresh control_repo if I am puppetserver or puppet CA
if [ "$1" == 'puppetca' ] || [ "$1" == 'puppetserver' ]; then

    mkdir -p ${PUPPET_KEYSDIR}
    # If defined, copy in EYAML keys from environment vars
    if [ "${PUPPET_EYAML_PUBKEY}" ] && [ "${PUPPET_EYAML_PUBKEY}" ]; then
	echo "${PUPPET_EYAML_PUBKEY}" > ${PUPPET_KEYSDIR}/public_key.pkcs7.pem
	echo "${PUPPET_EYAML_PRIVKEY}" > ${PUPPET_KEYSDIR}/private_key.pkcs7.pem
    fi
    chmod -R 0400 ${PUPPET_KEYSDIR}/*.pem
    chmod 0500 ${PUPPET_KEYSDIR}

    # If defined, copy in control_repo ssh key for root user from environment vars
    if [ "${PUPPET_CONTROLREPO_PRIVKEY}" ]; then
	echo "${PUPPET_CONTROLREPO_PRIVKEY}" > /root/.ssh/controlrepo_rsa
	chmod 0600 /root/.ssh/id_rsa
	cat <<SSHCONFIG >> /root/.ssh/config
Host ${PUPPET_CONTROLREPO_HOSTNAME}
	HostName ${PUPPET_CONTROLREPO_HOSTNAME}
	PreferredAuthentications publickey
	IdentityFile ~/.ssh/controlrepo_rsa
SSHCONFIG
    fi

    # Populate root known_hosts file
    ssh-keyscan ${PUPPET_CONTROLREPO_HOSTNAME} >> /root/.ssh/known_hosts 2>/dev/null;

   # Install git and curl if not already present
    yum -y install git curl

    # Update control_repo (we don't do this under Vagrant)
    cd ${SCRIPT_PATH}
    git pull
    git checkout ${PUPPET_ENVIRONMENT}
    cd ${CWD}
fi

if [ "$1" == 'puppetca' ]; then
    ## Bootstrap the puppetserver CA first
    ${SCRIPT_PATH}/bootstrap_puppetca.sh
elif [ "$1" == 'puppetserver' ]; then
    ## Bootstrap the puppetserver next
    ${SCRIPT_PATH}/bootstrap_puppetserver.sh
else
    ## Bootstrap a puppet node (including puppetdb)
    ${SCRIPT_PATH}/bootstrap_agent.sh
fi
