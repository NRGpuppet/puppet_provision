#!/bin/bash

# Provisioning script for CentOS 6/7 Puppet VM(s) under Virtualbox/vagrant.
#
# This script is intended to be able to run standalone by Vagrant, but it can accept the
# following global environment vars to override those specified in params.sh. Except
# where noted, these variables below must be defined.
#
# PUPPET_MASTER = FQDN or IP address of puppet master
# PUPPET_CA = for now, same as PUPPET_MASTER
# PUPPET_CONTROLREPO_URL = "git@bitbucket.org/NRGpuppet/puppet_control_public"
# PUPPET_CONTROLREPO_HOSTNAME = "bitbucket.org"
# PUPPET_CONTROLREPO_PRIVKEY = private RSA key granting access to the control_repo, optional
# PUPPET_CONTROLREPO_PUBKEY = control_repo public RSA key, for known_hosts file, optional
#
# These additional, optional environment variables can be used to pre-populate a keypair
# for hiera EYAML content.  Note these variables would override any EYAML keys already in
# provision/keys/eyaml.
#
# PUPPET_EYAML_PUBKEY = public PKCS7 EYAML key
# PUPPET_EYAML_PRIVKEY = private PKCS7 EYAML key

SCRIPT_PATH='/vagrant/provision'
OSRELEASE=$(rpm -qa \*-release | grep -Ei "redhat|centos" | cut -d"-" -f3)
source ${SCRIPT_PATH}/params.sh || (echo "Can't find params.sh" && exit 1)

# Restart syslog to reflect new hostname
case "$OSRELEASE" in
    '7' )
        systemctl restart rsyslog
        PUPPET_RELEASE_RPM="${PUPPET_RELEASE_RPM_EL7}"
        ;;
    '6' )
        service rsyslog restart
        PUPPET_RELEASE_RPM="${PUPPET_RELEASE_RPM_EL6}"
        ;;
    * )
        echo "This script doesn't support CentOS/RHEL ${OSRELEASE}"
        exit 1
        ;;
esac

# A reasonable PATH
echo "export PATH=${PATH}:/sbin:/usr/local/bin:${PUPPET_BINDIR}" >> /etc/bashrc

# Add host entries for each system
cat > /etc/hosts <<EOH
127.0.0.1 localhost localhost.localdomain localhost4 localhost4.localdomain
::1 localhost localhost.localdomain localhost6 localhost6.localdomain
192.168.137.10 xmaster.vagrant.vm xmaster puppet
192.168.137.12 xpuppetdb.vagrant.vm xpuppetdb puppetdb
192.168.137.13 xpuppetca.vagrant.vm xpuppetca puppetca
192.168.137.14 xagent.vagrant.vm xagent agent
192.168.137.15 xagentomnios.vagrant.vm xagentomnios agentomnios
EOH

# Force NTP time sync
case "$OSRELEASE" in
    '7' )
        yum install -y chrony
        systemctl enable chronyd
        systemctl start chronyd
        ;;
    '6' )
        yum install -y ntpdate
        chkconfig ntpdate on
        service ntpdate start
        ;;
esac

# Yum update, to appease yum_update repo
yum clean all
yum -y update | tee -a /var/log/puppet_deploy.log
mkdir -p /var/spool/check_yum_update
echo "$(( $(/bin/date +%s) / 86400 ))" > /var/spool/check_yum_update/last_update

# Update Puppet GPG Key
curl --remote-name --location https://yum.puppetlabs.com/RPM-GPG-KEY-puppet
rpm --import RPM-GPG-KEY-puppet

# Download and install puppet-agent if not already present
if [ ! -d "${PUPPET_BINDIR}" ]; then
    rpm -Uvh "${PUPPET_RELEASE_RPM}"
    yum -y install puppet-agent
fi

# Point puppet agent to puppet master/CA
[ ! "${PUPPET_CA}" ] && PUPPET_CA=${PUPPET_MASTER}
cat <<PUPPET >> ${PUPPET_CONFDIR}/puppet.conf
[main]
    ca_server = ${PUPPET_CA}
[agent]
    server = ${PUPPET_MASTER}
PUPPET

# Open the firwall on inter-VM network traffic
case "$OSRELEASE" in
    '7' )
        # Under CentOS 7 interface is enp0s8
        systemctl enable firewalld
        service firewalld start
        firewall-cmd --zone=trusted --change-interface=enp0s8
        firewall-cmd --runtime-to-permanent
        ;;
    '6' )
        # Not needed?
        ;;
esac

# This works around error on agent nodes when using non-production environment
# Ref: https://tickets.puppetlabs.com/browse/PUP-4954
mkdir -p ${PUPPET_ENVIRONMENTPATH}/${PUPPET_ENVIRONMENT}

# If present, copy in any custom RSA key for Vagrant root user.
mkdir -p ~/.ssh
chmod 0700 ~/.ssh
if [ -f /vagrant/provision/keys/ssh/vagrant_root.rsa ]; then
    cp /vagrant/provision/keys/ssh/vagrant_root.rsa ~/.ssh/id_rsa
    chmod 0600 ~/.ssh/id_rsa
fi

# Update keys if I am puppetserver or puppet CA
if [ "$1" == 'xpuppetca' ] || [ "$1" == 'xmaster' ]; then

    mkdir -p ${PUPPET_KEYSDIR}
    # If defined, copy in EYAML keys from environment vars
    if [ "${PUPPET_EYAML_PUBKEY}" ] && [ "${PUPPET_EYAML_PUBKEY}" ]; then
	echo "${PUPPET_EYAML_PUBKEY}" > ${PUPPET_KEYSDIR}/public_key.pkcs7.pem
	echo "${PUPPET_EYAML_PRIVKEY}" > ${PUPPET_KEYSDIR}/private_key.pkcs7.pem
    # Otherwise, copy in any EYAML keys from provision/keys if under vagrant
    elif [ -f /vagrant/provision/keys/eyaml/public_key.pkcs7.pem ]; then
	cp /vagrant/provision/keys/eyaml/*.pkcs7.pem ${PUPPET_KEYSDIR}
    fi
    chmod -R 0400 ${PUPPET_KEYSDIR}/*.pem
    chmod 0500 ${PUPPET_KEYSDIR}

    # If defined, copy in control_repo ssh key for root user from environment vars
    if [ "${PUPPET_CONTROLREPO_PRIVKEY}" ]; then
	echo "${PUPPET_CONTROLREPO_PRIVKEY}" > ~/.ssh/controlrepo_rsa
	chmod 0600 ~/.ssh/controlrepo_rsa
	cat <<SSHCONFIG >> ~/.ssh/config
Host ${PUPPET_CONTROLREPO_HOSTNAME}
	HostName ${PUPPET_CONTROLREPO_HOSTNAME}
	PreferredAuthentications publickey
	IdentityFile ~/.ssh/controlrepo_rsa
SSHCONFIG
    fi

    # Populate root known_hosts file
    if [ "${PUPPET_CONTROLREPO_PUBKEY}" ]; then
	echo "${PUPPET_CONTROLREPO_PUBKEY}" >> ~/.ssh/known_hosts
    else
	ssh-keyscan ${PUPPET_CONTROLREPO_HOSTNAME} >> ~/.ssh/known_hosts 2>/dev/null
    fi

    # Install git and curl if not already present
    yum -y install git curl

# I am puppet node, so check if a role was specified in $2
elif [ "$2" ]; then
    role="$2"
fi

# Create custom facts from environment vars, so they're available to hiera
mkdir -p /etc/facter/facts.d/
echo "puppet_server=${PUPPET_MASTER}" >> /etc/facter/facts.d/puppet_server.txt
echo "puppet_ca_server=${PUPPET_CA}" >> /etc/facter/facts.d/puppet_ca_server.txt

case "$1" in
    'xpuppetca' )
	## Bootstrap the puppetserver CA
	${SCRIPT_PATH}/bootstrap_puppetserver.sh "role::puppet::puppetca"
	;;
    'xmaster' )
	## Bootstrap the puppetserver
	${SCRIPT_PATH}/bootstrap_puppetserver.sh "role::puppet::puppetserver_int_db"
	;;
    * )
	## Bootstrap a puppet node (including puppetdb)
	${SCRIPT_PATH}/bootstrap_agent.sh "$role"
	;;
esac

echo "Vagrant provision finished! Access this instance using 'vagrant ssh ${1}', sudo to root!"
